FROM php:7-apache-buster
RUN a2enmod rewrite
RUN a2enmod actions
RUN a2enmod ssl
RUN cd /etc/apache2/sites-available/ \
 && a2ensite default-ssl.conf 
#RUN apt update \
# && apt upgrade -y \
# && apt install git unzip zlib1g-dev libpng-dev libwebp-dev libjpeg62-turbo-dev libxpm-dev libfreetype6-dev wget -y \
# && docker-php-ext-configure gd --with-webp --with-jpeg --with-xpm --with-freetype \
# && docker-php-ext-install gd 
RUN apt update \
 && apt upgrade -y \
 && apt install git unzip zlib1g-dev libpng-dev libwebp-dev libjpeg62-turbo-dev libxpm-dev libfreetype6-dev wget -y \
 && docker-php-ext-configure gd --with-webp --with-jpeg --with-xpm --with-freetype \
 && docker-php-ext-install gd \
 && curl https://getcomposer.org/installer > /root/composer-setup.php \
 && php /root/composer-setup.php --install-dir=/usr/local/bin/ --filename=composer
RUN cd /var/www \
 && git clone "https://github.com/typemill/typemill.git" ./html/ \
 && cd html \
 && composer update

RUN cd /var/www \
 #&& wget https://typemill.net/media/files/typemill-1.5.0.zip \
 && wget https://themes.typemill.net/download?themes=learn -O theme.zip

RUN cd /var/www \
# && unzip -u typemill-1.5.0.zip -d ./html/ \
 && unzip -u theme.zip -d ./html/themes/

RUN chown -R www-data:www-data /var/www/html \
    && find /var/www/html -type d -exec chmod 570 {} \; \
	&& find /var/www/html -type f -exec chmod 460 {} \;
RUN cp -R /var/www/html/content /var/www/html/content.orig
RUN openssl req -x509 -newkey rsa:4096 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem -days 365 -nodes -subj '/CN=localhost'
RUN sed -i 's/BlockApiController/ControllerAuthorBlockApi/g' /var/www/html/system/Controllers/ControllerAuthorMediaApi.php
COPY cmd.sh /
COPY init_content.sh /
RUN chown root:root /cmd.sh ; chmod 0700 /cmd.sh
RUN chown root:root /init_content.sh ; chmod 0700 /init_content.sh
CMD ["/cmd.sh"]
